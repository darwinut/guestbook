package com.example.models;

import javax.persistence.*;

/**
 * Created by Darwintartu on 16-Jun-16.
 */
@Entity
public class Comment {

    @Id

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;


    String comment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


}
