package com.example.models;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Darwintartu on 15-Jun-16.
 */
@Entity
public class GuestBook {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    Long id;

    String name;
    String email;

    @OneToMany(cascade = {CascadeType.ALL, CascadeType.PERSIST, CascadeType.REFRESH})
    List<Comment> comments;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
