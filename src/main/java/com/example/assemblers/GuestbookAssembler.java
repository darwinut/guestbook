package com.example.assemblers;

import com.example.dtos.CommentDTO;
import com.example.dtos.GuestbookDTO;
import com.example.models.Comment;
import com.example.models.GuestBook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Darwintartu on 18-Jun-16.
 */
@Service
public class GuestbookAssembler {

    @Autowired
    CommentAssembler commentAssembler;

    public GuestbookDTO toDTO(GuestBook guestBook){
        GuestbookDTO dto = new GuestbookDTO();
        dto.setId(guestBook.getId());
        dto.setName(guestBook.getName());
        dto.setEmail(guestBook.getEmail());

        List<CommentDTO> commentDTOs = new ArrayList<>();

        List<Comment> comments = guestBook.getComments();
        for(Comment comment: comments){
            CommentDTO commentDTO = new CommentDTO();
            commentDTO.setComment(comment.getComment());
            commentDTOs.add(commentDTO);
        }
        dto.setComments(commentDTOs);
        return dto;
    }
}
