package com.example.assemblers;

import com.example.dtos.CommentDTO;
import com.example.models.Comment;
import org.springframework.stereotype.Service;

/**
 * Created by Darwintartu on 18-Jun-16.
 */
@Service
public class CommentAssembler {

    public CommentDTO toDTO(Comment comment){
        CommentDTO dto = new CommentDTO();
        dto.setId(comment.getId());
        dto.setComment(comment.getComment());
        return dto;
    }
}
