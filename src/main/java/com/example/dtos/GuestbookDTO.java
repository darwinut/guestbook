package com.example.dtos;

import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by Darwintartu on 18-Jun-16.
 */

public class GuestbookDTO {
    Long id;
    String name;
    String email;

    List<CommentDTO> comments;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<CommentDTO> getComments() {
        return comments;
    }

    public void setComments(List<CommentDTO> comments) {
        this.comments = comments;
    }
}
