package com.example.dtos;

/**
 * Created by Darwintartu on 18-Jun-16.
 */

public class CommentDTO {

    Long id;
    String comment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
