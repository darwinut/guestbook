package com.example.services;

import com.example.assemblers.GuestbookAssembler;
import com.example.dtos.CommentDTO;
import com.example.dtos.GuestbookDTO;
import com.example.models.Comment;
import com.example.models.GuestBook;
import com.example.repositories.GuestBookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Darwintartu on 18-Jun-16.
 */
@Service
public class GuestBookService {

    @Autowired
    GuestBookRepository gbRepo;

    @Autowired
    GuestbookAssembler gbAssembler;

    public List<Comment> toComments(List<CommentDTO> commentDTOs){
        List<Comment> comments = new ArrayList<>();
        for(CommentDTO commentDTO: commentDTOs){
            Comment comment = new Comment();
            comment.setComment(commentDTO.getComment());
            comments.add(comment);
        }
        return comments;
    }
    public void updateCommentReply(GuestbookDTO guestbookDTO){
        GuestBook guestBook = new GuestBook();
        guestBook.setId(guestbookDTO.getId());
        guestBook.setName(guestbookDTO.getName());
        guestBook.setEmail(guestbookDTO.getEmail());
        guestBook.setComments(toComments(guestbookDTO.getComments()));
        gbRepo.save(guestBook);

    }

    public void saveComment(GuestbookDTO guestbookDTO){
        GuestBook guestBook = new GuestBook();
        guestBook.setName(guestbookDTO.getName());
        guestBook.setEmail(guestbookDTO.getEmail());
        List<Comment> comments = new ArrayList<>();
        comments = toComments(guestbookDTO.getComments());
        guestBook.setComments(comments);
        gbRepo.save(guestBook);
    }

    public List<GuestbookDTO> getAllGuestEntries(){
        List<GuestBook> guestbooks = gbRepo.findAll();
        List<GuestbookDTO> guestbookDTOs = new ArrayList<>();
        for(GuestBook guestbook: guestbooks){
            guestbookDTOs.add(gbAssembler.toDTO(guestbook));
        }
        return guestbookDTOs;
    }

    public GuestbookDTO getOneEntry(Long id) {
        GuestBook guestBook = gbRepo.findOne(id);
        GuestbookDTO guestbookDTO = gbAssembler.toDTO(guestBook);
        return guestbookDTO;
    }

}
