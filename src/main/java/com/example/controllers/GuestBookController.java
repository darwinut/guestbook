package com.example.controllers;

import com.example.assemblers.CommentAssembler;
import com.example.assemblers.GuestbookAssembler;
import com.example.dtos.GuestbookDTO;
import com.example.models.Comment;
import com.example.dtos.CommentDTO;
import com.example.models.GuestBook;
import com.example.repositories.CommentRepository;
import com.example.repositories.GuestBookRepository;
import com.example.services.GuestBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Darwintartu on 15-Jun-16.
 */

@Controller
public class GuestBookController {

    @Autowired
    GuestBookService guestBookService;

    @Autowired
    GuestbookAssembler guestbookAssembler;

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public String getGuests(Model model){
        GuestbookDTO guestBookDTO = new GuestbookDTO();
        model.addAttribute("guest", guestBookDTO);
        CommentDTO commentDTO = new CommentDTO();
        model.addAttribute("comment_in", commentDTO);
        List<GuestbookDTO> guestbookDTOs = guestBookService.getAllGuestEntries();
        model.addAttribute("guests", guestbookDTOs);
        return "index";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/guests")
    public String addEntry(GuestbookDTO guestbookDTO){
        guestBookService.saveComment(guestbookDTO);
        return "redirect:/";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/guests/{id}/comments")
    public String addNewComment(@PathVariable Long id, CommentDTO commentDTO){
        GuestbookDTO guestbookDTO = guestBookService.getOneEntry(id);
        guestbookDTO.getComments().add(commentDTO);
        guestBookService.updateCommentReply(guestbookDTO);
        return "redirect:/";
    }

}
