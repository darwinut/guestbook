package com.example.repositories;

import com.example.models.GuestBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Darwintartu on 15-Jun-16.
 */
@Repository
public interface GuestBookRepository extends JpaRepository<GuestBook, Long>{
}
